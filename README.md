#HMH content bundler

##intro 
The HMH content bundler is a tool that takes one or more download manifest, 
gets all the resources from them and creates one tar file with all the resources bundled up.

The tool is build using node js stack.


## Install
- install node 
- clone repo
- run `npm install`


## How to use
- run `node cmd 
--manifestPath {path to manifest} 
--outputPath {output folder for new tars and manifest} 
--serverUrl {the url to the server where the content is on} 
--baseRelativePath {relative path that will be added at the begining for every new download item}`

example: `node cmd --manifestPath "./test/stuff/math-manifest.json" --outputPath "./output" --serverUrl "https://my-review-cert.hrw.com" --baseRelativePath ""`