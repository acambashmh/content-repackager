'use strict';
const Promise = require('bluebird');
const getStaticResource = require('./getStaticResource');

const brightcoveUrl = 'https://api.brightcove.com/services/library';
const httpreq = require('httpreq');
let video_token = 'cLsCcy1dDwolkR0UPKf7zJ8ZyU13B7EStFY1k-GqiAMxbQu1pk2Uug..';


const getVideoResource = (downloadItem, outputFolder) => {

    if (!downloadItem || !downloadItem.package_details || !downloadItem.package_details.api_details
        || !downloadItem.package_details.api_details.api_parameters || !downloadItem.package_details.api_details.api_parameters[0]
        || !downloadItem.package_details.api_details.api_parameters[0].value) {
        return Promise.reject(new Error('Video download item doesnt have correct parameters: ' + JSON.stringify(downloadItem, null, 2)));
    }
    const referenceId = downloadItem.package_details.api_details.api_parameters[0].value;

    return new Promise((resolve, reject) => {


        if (downloadItem.package_details.api_details.api_parameters[0].token) {
            video_token = downloadItem.package_details.api_details.api_parameters[0].token;
        }
        const params = {
            command: downloadItem.package_details.api_details.api_parameters[0].key == 'video_id' ? 'find_video_by_id' : 'find_video_by_reference_id',
            reference_id: referenceId, // this is intended to be used as the destination file name
            media_delivery: 'HTTP',
            video_fields: 'FLVURL,videoStillURL,captioning,renditions,iOSRenditions,WVMRenditions,referenceId,thumbnailUrl',
            token: video_token
        };

        httpreq.get(brightcoveUrl, { parameters: params }, (err, response) => {
            if (err) {
                return reject(err);
            }
            try {
                if (!response.body || response.body === 'null') {
                    return reject(new Error('brightcove data is not returned: ' + JSON.stringify(downloadItem, null, 2)));
                }
                const result = JSON.parse(response.body);
                resolve(result);
            } catch (e) {
                reject(e);
            }
        });
    }).then((data) => {
        const videoFilesUrls = [];
        if (!data.FLVURL) {
            throw new Error('brightcove video query results missing FLVURL');
        }

        //videoFilesUrls.push(data.FLVURL);
        videoFilesUrls.push({
            url: data.FLVURL,
            name: referenceId + '.mp4'
        });

        if (data.videoStillURL) {
            //videoFilesUrls.push(data.videoStillURL);
            videoFilesUrls.push({
                url: data.videoStillURL,
                name: referenceId + '.jpg'
            });

        }
        if (data.thumbnailURL) {
            //videoFilesUrls.push(data.thumbnailURL);
            videoFilesUrls.push({
                url: data.thumbnailURL,
                name: referenceId + '-thumb.jpg'
            });
        }
        if (data.captioning && data.captioning.captionSources && data.captioning.captionSources[0]) {
            var captionsUrl = data.captioning.captionSources[0].url;

            videoFilesUrls.push({
                url: captionsUrl,
                name: referenceId + '.xml'
            });
        }

        var tempPromiseList = videoFilesUrls.map((item) => {
            return {
                fileName: item.name,
                download_priority: downloadItem.download_priority,

                package_details: {
                    download_uri: {
                        relative_url: item.url
                    },
                    target_path: {
                        relative_url: downloadItem.package_details.target_path.relative_url
                    }
                }
            };
        })
            .map((downloadItem) => {
                return getStaticResource(downloadItem, outputFolder, null, downloadItem.fileName);
            });
        return Promise.all(tempPromiseList);
    });
};

module.exports = getVideoResource;