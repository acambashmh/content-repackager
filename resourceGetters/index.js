const getTar = require('./getTar');
const getStaticResource = require('./getStaticResource');
const getBrightcoveVideo = require('./getBrightcoveVideo');
const getPMT = require('./getPMT');
const getPmtPlayer = require('./getPMTPlayer');



module.exports = {
    getTar,
    getStaticResource,
    getBrightcoveVideo,
    getPMT,
    getPmtPlayer
};