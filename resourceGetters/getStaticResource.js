'use strict';
const httpreq = require('httpreq');
const Promise = require('bluebird');
const path = require('path');
var fs = Promise.promisifyAll(require('fs-extra'));
var getFileName = require('../util/getFileName');
var resourceGetterResult = require('../model/resourceGetterResult');
var constructPathOnPriority = require('../util/constructPathOnPriority');

module.exports = (downloadItem, outputFolder, serverUrl, customFileName) => {

    return new Promise((resolve, reject) => {
        if (!downloadItem || !downloadItem.package_details || !downloadItem.package_details.target_path) {
            return reject(new Error('download item is not valid: ' + JSON.stringify(downloadItem, null, 2)));
        }
        const downloadPath = constructPathOnPriority(outputFolder, downloadItem.package_details.target_path.relative_url, downloadItem.download_priority);
        return fs.ensureDirAsync(downloadPath)
            .then(() => {
                resolve(downloadPath);
            });

    }).then((downloadPath) => {
        return new Promise((resolve, reject) => {

            let fileName = '';
            if (customFileName) {
                fileName = customFileName;
            } else {
                fileName = getFileName(downloadItem.package_details.download_uri.relative_url);
            }

            let downloadUrl = null;
            if (serverUrl) {
                downloadUrl = serverUrl + downloadItem.package_details.download_uri.relative_url;
            } else {
                downloadUrl = downloadItem.package_details.download_uri.relative_url;
                if (downloadUrl.indexOf('//') === 0) {
                    downloadUrl = 'http:' + downloadUrl;
                }
            }
            const fullDownloadPath = path.join(downloadPath, fileName);

            httpreq.download(downloadUrl, fullDownloadPath, (err) => {
                if (err) {
                    return reject(err);
                }
            }, (err, res) => {
                if (err) {
                    return reject(err);
                } else if (res.statusCode !== 200) {
                    return reject(new Error('File not found, url: ' + downloadUrl));
                }
                const relativeOnDiskPath = constructPathOnPriority('', path.join(downloadItem.package_details.target_path.relative_url, fileName), downloadItem.download_priority);

                resolve(resourceGetterResult(downloadItem.download_priority, [relativeOnDiskPath]));
            });
        });
    });
};