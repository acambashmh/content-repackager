const getTarResource = require('./getTar.js');
const Promise = require('bluebird');
const httpreq = require('httpreq');
var parseString = require('xml2js').parseString;



const getPMT = (downloadItem, outputFolder, serverUrl) => {
    return new Promise((resolve, reject) => {

        var xmlURL = serverUrl + '/wwtb/offline/packer.pl?';
        var params = downloadItem.package_details.api_details.api_parameters.reduce((curr, next) => {
            if (curr !== '') {
                curr += '&';
            }
            curr += next.key + '=' + next.value;
            return curr;
        }, '');

        xmlURL += params;
        httpreq.get(xmlURL, (err, response) => {
            if (err) {
                return reject(err);
            }
            parseString(response.body, function (err, xmlConversionRes) {
                if (err) {
                    reject(err);
                }
                var pmtParams = {
                    wid: xmlConversionRes.packer.wid[0],
                    targetdir: xmlConversionRes.packer.targetdir[0],
                    url: xmlConversionRes.packer.url[0],
                    pmturl: xmlConversionRes.packer.pmturl[0]
                };
                resolve(pmtParams);
            });

        });
    }).then((pmtParams) => {
        const pmtPackageItem = {
            download_priority: downloadItem.download_priority,

            package_details: {
                download_uri: {
                    relative_url: pmtParams.url
                },
                target_path: {
                    relative_url: pmtParams.targetdir
                }
            }
        };
        return getTarResource(pmtPackageItem, outputFolder);
    });
};

module.exports = getPMT;