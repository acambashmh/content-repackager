'use strict';
const Promise = require('bluebird');
const httpreq = require('httpreq');
const path = require('path');
const parseString = require('xml2js').parseString;



const getPMT = (downloadItem, outputFolder, serverUrl) => {
    return new Promise((resolve, reject) => {

        var xmlURL = serverUrl + '/wwtb/offline/packer.pl?';
        var params = downloadItem.package_details.api_details.api_parameters.reduce((curr, next) => {
            if (curr !== '') {
                curr += '&';
            }
            curr += next.key + '=' + next.value;
            return curr;
        }, '');

        xmlURL += params;
        httpreq.get(xmlURL, (err, response) => {
            if (err) {
                return reject(err);
            }
            parseString(response.body, function (err, xmlConversionRes) {
                if (err) {
                    reject(err);
                }

                const pmtParam = {
                    pmturl : xmlConversionRes.packer.pmturl[0],
                    version : xmlConversionRes.packer.pmtversion[0]
                };

                resolve(pmtParam);
            });

        });
    }).then((pmtParam) => {
        return new Promise((resolve, reject) => {
            let pmturl = pmtParam.pmturl;
            if(pmturl.indexOf('//') === 0){
                pmturl = 'http:' + pmturl;
            }
            httpreq.download(pmturl, path.join(outputFolder, 'pmtPlayer.tar'),  (err) => {
                if(err){
                    reject(new Error('error downloading pmt player: ' + JSON.stringify(err, null, 2)));
                }
                resolve(pmtParam.version);
            });

        });
    });
};

module.exports = getPMT;