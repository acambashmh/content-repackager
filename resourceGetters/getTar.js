const getStaticResource = require('./getStaticResource');
const resourceGetterResult = require('../model/resourceGetterResult');
const path = require('path');
const Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs-extra'));
var tar = require('tar');

const getTar = (downloadItem, outputFolder, serverUrl) => {
    return getStaticResource(downloadItem, outputFolder, serverUrl)
        .then((staticFileOutputData) => {
            const pathToTheTar = path.join(outputFolder, staticFileOutputData.filePaths[0]);
            return new Promise((resolve, reject) => {
                const outputPath = path.join(pathToTheTar, '../');

                function onError(err) {
                    reject(err);
                }

                const fileList = [];

                var extractor = tar.Extract({ path: outputPath })
                    .on('error', onError)
                    .on('entry', (entry) => {
                        if (entry.type !== 'Directory') {
                            //const tempPath = path.join(staticFileOutputData.filePaths[0], entry.props.path);
                            //fileList.push(tempPath);
                        }
                    })
                    .on('end', () => {
                        resolve({
                            pathToTheTar,
                            fileList
                        });
                    });

                fs.createReadStream(pathToTheTar)
                    .on('error', onError)
                    .pipe(extractor);
            });
        })
        .then((tempData) => {
            return fs.removeAsync(tempData.pathToTheTar).then(() => {
                return resourceGetterResult(downloadItem.download_priority, tempData.fileList);
            });
        });

};

module.exports = getTar;
