const repackage = require('../repackager');
var argv = require('yargs').argv;
const winston = require('winston');
//(manifestPath, outputPath, serverUrl, baseRelativePath)



if(!argv.manifestPath){
    winston.error('manifestPath param is not defined');
    throw new Error('manifestPath param is not defined');
}else{
    winston.log('info', `manifestPath is: ${argv.manifestPath}`);
}

if(!argv.outputPath){
    winston.error('outputPath param is not defined');
    throw new Error('outputPath param is not defined');
}else{
    winston.log('info', `outputPath is: ${argv.outputPath}`);
}

if(!argv.serverUrl){
    winston.error('serverUrl param is not defined');
    throw new Error('serverUrl param is not defined');
}else{
    winston.log('info', `serverUrl is: ${argv.serverUrl}`);
}

if(argv.baseRelativePath){
    winston.warn('baseRelativePath param is not defined');
} else{
    winston.log('info', `baseRelativePath is: ${argv.baseRelativePath}`);
}


repackage(argv.manifestPath, argv.outputPath, argv.serverUrl, argv.baseRelativePath).then(() => {
    winston.log('info', 'MANIFEST REPACKAGING IS DONE');
});

