'use strict';
const downloadItemsExtractor = require('../downloadItemsExtractor');
const saveManifest = require('../manfestSaver');
const util = require('../util');
const path = require('path');
const manifestGetter = require('../manifestGetter');
const logger = require('../util').logger;
const Promise = require('bluebird');

const repackage = (manifestPath, outputPath, serverUrl, baseRelativePath) => {
    logger.log(`repackaging started:  manifestPath:${manifestPath} outputPath: ${outputPath} serverUrl:${serverUrl} baseRelativePath:${baseRelativePath}`);
    let manifestData = null;

    return manifestGetter(manifestPath).then((result) => {
        if (!result) {
            throw new Error('error with getting manifest data');
        }
        logger.log('old manifest downloaded');
        manifestData = result;

    }).then(() => {

        if (!manifestData || !manifestData.download_packages || !manifestData.download_packages.downloads
            || manifestData.download_packages.downloads.length === 0) {
            return Promise.reject(new Error('there is no manifest data'));
        }

        if (!outputPath) {
            return Promise.reject(new Error('output path for the new generated packages is not defined'));
        }

        if (!serverUrl) {
            return Promise.reject(new Error('server from which the data is supposed to be downloaded is not defined'));
        }

        return downloadItemsExtractor(manifestData.download_packages.downloads, outputPath, serverUrl, baseRelativePath);
    }).then((newDownloadItems) => {

        manifestData.download_packages.downloads = newDownloadItems;

        const fullNewManifestPath = path.join(outputPath, util.getFileName(manifestPath));

        return saveManifest(manifestData, fullNewManifestPath);
    }).then(() => {
        logger.log('manifest items repackaged');
    });
};



module.exports = repackage;
