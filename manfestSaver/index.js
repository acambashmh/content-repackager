const Promise = require('bluebird');
const fs = require('fs');
const logger = require('../util').logger;


module.exports = (downloadManifest, path) => {

    return new Promise((resolve, reject) => {
        fs.writeFile(path, JSON.stringify(downloadManifest,null,2), (err) => {
            if(err){
                return reject(err);
            }
            logger.log(`file saved on path ${path}`);
            resolve();
        });
    });

    //return fs.writeFileAsync(path, JSON.stringify(downloadManifest,null,2));
};
