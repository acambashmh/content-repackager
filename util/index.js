const getFileName = require('./getFileName');
const constructPathOnPriority = require('./constructPathOnPriority');
const logger = require('./logger');
const walk = require('./walk');


module.exports = {
    getFileName,
    constructPathOnPriority,
    logger,
    walk
};