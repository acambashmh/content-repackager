module.exports = (path) => {
    if(!path){
        throw new Error('url to a file was not provided');
    }


    if(path.indexOf('/') === -1){
        return path;
    }else {
        const splitArray = path.split('/');
        return splitArray[splitArray.length-1];
    }
};