var Promise = require('bluebird');
const fs = require('fs-extra');


module.exports = (dirPath) => {
    return new Promise((resolve, reject) => {
        var items = [];
        fs.walk(dirPath)
            .on('data', function (item) {
                items.push(item.path);
            })
            .on('error', function (item) {
                reject(new Error('error with getting the list of files for dir: ' + dirPath));
            })
            .on('end', function () {
                resolve(items);
            });

    });

};