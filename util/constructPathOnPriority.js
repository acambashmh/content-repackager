var path = require('path');

module.exports = (outputFolder, relativeUrl, download_priority) => {
    if(!download_priority && download_priority !==0){
        return path.join(outputFolder, relativeUrl);
    }

    const isCoreResource =  download_priority <= 1;
    if(isCoreResource){
        return path.join(outputFolder, 'core', relativeUrl);
    }else{
        return path.join(outputFolder, 'additional', relativeUrl);
    }
};