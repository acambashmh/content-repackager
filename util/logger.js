'use strict';

const winston = require('winston');

let logger;

let setUpConsoleLog = (process.env.NODE_ENV === 'development');

setUpConsoleLog = true;


if(setUpConsoleLog){
    logger = new (winston.Logger)({
        transports: [
            new (winston.transports.Console)()
        ]
    });
}else{
    logger = new (winston.Logger)({

    });
}





module.exports = {
    info: (text) => {
        logger.log('info', text);
    },
    log: (text) => {
        logger.log('info', text);
    },
    warn: (text) => {
        logger.log('warn', text);
    },
    error: (text) => {
        logger.log('error', text);
    }
};
