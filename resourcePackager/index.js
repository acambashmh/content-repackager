const Promise = require('bluebird');
const path = require('path');
const archiver = require('archiver');
const fs = Promise.promisifyAll(require('fs-extra'));
const logger = require('../util').logger;

const packageFolder = (dirPath, outputPath, cleanFolder) => {
    return fs.readdirAsync(dirPath).then((folders) => {
        return new Promise((resolve, reject) => {

            var output = fs.createWriteStream(outputPath);
            var archive = archiver('tar');

            output.on('close', function() {
                resolve();
            });

            archive.on('error', function(err) {
                reject(err);
            });

            archive.pipe(output);

            folders.forEach((folder) => {
                archive.directory(path.join(dirPath), '');
            });

            archive.finalize(function(err) {
                if (err) {
                    reject(err);
                }
            });

        });
    }).then(() => {
        if(cleanFolder){
            return fs.removeAsync(dirPath);
        }
    });
};


module.exports = packageFolder;
