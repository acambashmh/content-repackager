module.exports = ( download_priority, filePaths) => {
    const isCoreResource =  download_priority <= 1;
    return {
        isCoreResource,
        filePaths
    };
    
};