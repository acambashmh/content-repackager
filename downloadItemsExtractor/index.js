const Promise = require('bluebird');
const handleCoreResources = require('./handleCoreResources');
const handleGoPlayer = require('./handleGoPlayer');
const handleAdditionalResources = require('./handleAdditionalResources');
const handlePmtPlayer = require('./handlePmtPlayer');
const logger = require('../util').logger;

const parseManifest = (oldDownloadItems, outputPath, serverUrl, baseRelativePath) => {

    if(!oldDownloadItems || oldDownloadItems.length === 0){
        return Promise.reject('no download items to repackage');
    }

    const handlers = [handleCoreResources, handleGoPlayer, handleAdditionalResources, handlePmtPlayer];

    const newManifestItems = [];
    return handlers.reduce((currPromise,handler) => {
        return currPromise.then(() => {
            logger.log('item extractor started');
            return handler(oldDownloadItems, outputPath, serverUrl, baseRelativePath)
                .then((downloadItem) => {
                    logger.log('item extractor ended');
                    if(downloadItem){
                        newManifestItems.push(downloadItem);
                    }
                });
        });
    }, Promise.resolve()).then(() => {
        logger.log('all item extractors are finished');
        return newManifestItems;
    });

};

module.exports = parseManifest;
