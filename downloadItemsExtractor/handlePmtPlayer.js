'use strict';
const Promise = require('bluebird');
const getPmtPlayer = require('../resourceGetters');
const logger = require('../util').logger;


const handlePmtPlayer = (downloadItems, outputFolder, server, baseRelativePath) => {

    if(!downloadItems || downloadItems.length === 0){
        return Promise.resolve();
    }

    var hasPmt = downloadItems.filter((item) => {
        return item.type === 'pmt';
    });

    if(!hasPmt || hasPmt.length === 0){
        return Promise.resolve();
    }
    logger.log('starting to download pmt player');
    return getPmtPlayer.getPmtPlayer(hasPmt[0], outputFolder, server).then((version) => {

        const temp = {
            download_priority: 0,
            description: 'Pmt player',
            type: 'player',
            version: version,
            required: true,
            download_if_present: false,
            mime_type: 'application/x-tar',
            package_size: 0,
            role: 'student',
            package_details: {
                download_uri: {
                    relative_url: baseRelativePath + '/pmtPlayer.tar',
                    server_url: 'http://my.hrw.com'
                },
                target_path: {
                    relative_url: '/wwtb/offlinedata/'
                }
            }
        };

        logger.log(`finished downloading pmt player, download item: ${JSON.stringify(temp, null,2)}`);

        return  temp;
    });
};


module.exports = handlePmtPlayer;
