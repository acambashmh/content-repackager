const resourceGetters = require('../resourceGetters');
const resourcePacker = require('../resourcePackager');
const Promise = require('bluebird');
const path = require('path');
const logger = require('../util').logger;


const filterCoreContent = (downloadItems) => {
    return downloadItems.filter((item) => {
        if((item.type === 'content' || item.type === 'brightcove'|| item.type === 'pmt' ) && item.download_priority <=1){
            return true;
        }
        return false;
    });
};


    const handleCoreResources = (downloadItems, outputFolder, server, baseRelativePath) => {
    const coreItems = filterCoreContent(downloadItems);

    if(!coreItems || coreItems.length === 0){
        logger.warn('there are no core resources for this manifest');
        return new Promise.resolve();
    }

    const promiseList = coreItems.map((downloadItem) => {
        if(downloadItem.type === 'content' && downloadItem.mime_type.indexOf('tar') === -1){
            return resourceGetters.getStaticResource(downloadItem, outputFolder, server);
        } else if (downloadItem.type === 'content' && downloadItem.mime_type.indexOf('tar') !== -1) {
            return resourceGetters.getTar(downloadItem, outputFolder, server);
        } else if (downloadItem.type === 'pmt') {
            return resourceGetters.getPMT(downloadItem, outputFolder, server);
        } else if (downloadItem.type === 'brightcove') {
            return resourceGetters.getBrightcoveVideo(downloadItem, outputFolder);
        }else{
            Promise.reject(new Error('resource should not be part of core resources: ' + JSON.stringify(downloadItem, null, 2)));
        }
    });

    return Promise.all(promiseList).then(() => {
        return resourcePacker(path.join(outputFolder, '/core'), path.join(outputFolder, '/core.tar'),true );
    }).then(() => {
        const temp = {
            download_priority: 0,
            description: 'Core files',
            type: 'content',
            version: '1.0',
            required: true,
            download_if_present: false,
            mime_type: 'application/x-tar',
            package_size: 0,
            role: 'student',
            package_details: {
                download_uri: {
                    relative_url: baseRelativePath + '/core.tar',
                    server_url: 'http://my.hrw.com'
                },
                target_path: {
                    relative_url: '/'
                }
            }
        };

        logger.log(`finished downloading and packaging core items, downloadItem: ${JSON.stringify(temp, null,2)}`);
        return temp;
    });

};

module.exports = handleCoreResources;

//var data = require('C:/Users/cambasa/Desktop/mma/test/stuff/math-manifest.json');
//
// handleCoreResources(data.download_packages.downloads, 'C:/Users/cambasa/Desktop/mma/output','https://my-review-cert.hrw.com').then((res) => {
//     console.log(res);
// }).catch(console.log);
