'use strict';
const Promise = require('bluebird');
const getPmtPlayer = require('../resourceGetters');
const logger = require('../util').logger;



const handlePlayer = (downloadItems, outputFolder, server, baseRelativePath) => {

    if (!downloadItems || downloadItems.length === 0) {
        return Promise.resolve();
    }

    var hasPmt = downloadItems.filter((item) => {
        return item.type === 'player';
    });

    if (!hasPmt || hasPmt.length === 0) {
        return Promise.resolve();
    }

    const playerDownloadItem = hasPmt[0];

    const downloadStaticConfig = {
        package_details: {
            download_uri: {
                relative_url: playerDownloadItem.package_details.download_uri.relative_url
            },
            target_path: {
                relative_url: '/'
            }
        }
    };

    logger.log('starting to download and package go player');

    return getPmtPlayer.getStaticResource(downloadStaticConfig, outputFolder, server, 'goPlayer.tar').then(() => {
        playerDownloadItem.package_details.download_uri.relative_url = baseRelativePath + '/goPlayer.tar';
        logger.log(`finished downloading and packaging go player, download item: ${JSON.stringify(playerDownloadItem,null,2)}`);
        return playerDownloadItem;
    });
};


module.exports = handlePlayer;

//
// var data = require('C:/Users/cambasa/Desktop/mma/test/stuff/math-manifest.json');
//
// handlePlayer(data.download_packages.downloads, 'C:/Users/cambasa/Desktop/mma/output','https://my-review-cert.hrw.com').then((res) => {
//     console.log(res);
// }).catch((err) => {
//     console.log(err);
// });
