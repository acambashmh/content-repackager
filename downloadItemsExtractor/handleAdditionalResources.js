const resourceGetters = require('../resourceGetters');
const resourcePacker = require('../resourcePackager');
const Promise = require('bluebird');
const path = require('path');
const logger = require('../util').logger;


const filterAdditionalContent = (downloadItems) => {
    return downloadItems.filter((item) => {
        if((item.type === 'content' || item.type === 'brightcove'|| item.type === 'pmt' ) && item.download_priority > 1){
            return true;
        }
        return false;
    });
};


const handleAdditionalResources = (downloadItems, outputFolder, server,baseRelativePath) => {
    const coreItems = filterAdditionalContent(downloadItems);

    if(!coreItems || coreItems.length === 0){
        logger.warn('there are no additional resources for this manifest');
        return new Promise.resolve();
    }

    logger.log(`starting to collect ${coreItems.length} core item(s)`);

    const promiseList = coreItems.map((downloadItem) => {
        if(downloadItem.type === 'content' && downloadItem.mime_type.indexOf('tar') === -1){
            return resourceGetters.getStaticResource(downloadItem, outputFolder, server);
        } else if (downloadItem.type === 'content' && downloadItem.mime_type.indexOf('tar') !== -1) {
            return resourceGetters.getTar(downloadItem, outputFolder, server);
        } else if (downloadItem.type === 'pmt') {
            return resourceGetters.getPMT(downloadItem, outputFolder, server);
        } else if (downloadItem.type === 'brightcove') {
            return resourceGetters.getBrightcoveVideo(downloadItem, outputFolder);
        }else{
            Promise.reject(new Error('resource should not be part of core resources: ' + JSON.stringify(downloadItem, null, 2)));
        }
    });

    return Promise.all(promiseList).then(() => {
        return resourcePacker(path.join(outputFolder, '/additional'), path.join(outputFolder, '/additional.tar'),true );
    }).then(() => {
        const temp = {
            download_priority: 2,
            description: 'Additional files',
            type: 'content',
            version: '1.0',
            required: true,
            download_if_present: false,
            mime_type: 'application/x-tar',
            package_size: 0,
            role: 'student',
            package_details: {
                download_uri: {
                    relative_url: baseRelativePath + '/additional.tar',
                    server_url: 'http://my.hrw.com'
                },
                target_path: {
                    relative_url: '/'
                }
            }
        };

        logger.log(`finished downloading and packaging additional items, downloadItem: ${JSON.stringify(temp, null,2)}`);

        return temp;
    });

};

module.exports = handleAdditionalResources;
