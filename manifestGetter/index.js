const fs = require('fs');
const Promise = require('bluebird');
const httpreq = require('httpreq');
const logger = require('../util').logger;


const isLocal = (path) => {
    return !(path.indexOf('http') === 0);
};

const getDownloadManifest = (path) => {
    if(!path){
        return Promise.reject(new Error('path to manifest is not provided'));
    }

    return new Promise((resolve, reject) => {
        if(isLocal(path)){
            fs.readFile(path,(err, data) => {
                if(err){
                    return reject(err);
                }
                try {
                    resolve(JSON.parse(data));
                }catch (e){
                    return reject(e);
                }
            });
        } else {
            httpreq.get(path, (err, response) => {
                if(err){
                    return reject(err);
                }
                try {
                    resolve(JSON.parse(response.body));
                }catch (e){
                    return reject(e);
                }
            });
        }

    });
};

module.exports = getDownloadManifest;



// getDownloadManifest('C:/Users/cambasa/Desktop/mma/test/stuff/math-manifest.json').then((data) => {
//     console.log(data);
// }).catch((err) => {
//     console.log(err);
// });